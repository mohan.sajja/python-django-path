## Conceptual understanding:


* https://medium.freecodecamp.org/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b

## Pycon videos on Docker (Optional):

* https://www.youtube.com/watch?v=GVVtR_hrdKI
* https://www.youtube.com/watch?v=9bvdc55xYdo

## Installation on Ubuntu:

* https://docs.docker.com/install/linux/docker-ce/ubuntu/

(You can install Docker on an EC2 instance instead of installing it on your laptop)

## Tutorial:


* Docker tutorial - https://docs.docker.com/get-started/

(Parts 1 through 3)

## docker-compose:

* Installation: https://docs.docker.com/compose/install/

* docker-compose is configured using YAML - Learn the yaml file format at https://learnxinyminutes.com/docs/yaml/

* Tutorial - https://docs.docker.com/compose/gettingstarted/




