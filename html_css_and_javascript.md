## HTML and CSS

### Start with Learn to Code HTML and CSS by Shay Howe

In particular read the following sections and complete the exercises.

HTMl & CSS

Lesson 1: Building your first web page
Lesson 2: Getting to know HTML
Lesson 3: Getting to know CSS
Lesson 4: Opening the Box Model
Lesson 5: Positioning content

Advanced HTML & CSS

Lesson 3: Complex Selectors
Lesson 4: Responsive Web Design


CSS Flex - http://css-tricks.com/snippets/css/a-guide-to-flexbox/

CSS Grid - https://css-tricks.com/snippets/css/complete-guide-grid/

### Project - Responsive News Website - https://github.com/mountblue/javascript-full-stack-path#projects


### Important concepts

* Box Model
* block elements, inline elements, inline-block?
* margin, padding - What's the difference?
* Units - vh, vw, em, px, rem vs em
* position - absolute, relative, static, fixed. What do they mean and when are they used
* Responsive Design - What is Responsive design, media queries. 
* Layout - How to arrange elements according to proposed design, flexbox

## JavaScript:

### JavaScript Basics
[Introduction to JavaScript - Udacity](https://in.udacity.com/course/intro-to-javascript--ud803)


### JavaScript and the browser
Asynchronous programming

#### Callbacks, Promises, Async

* https://codeburst.io/javascript-what-the-heck-is-a-callback-aba4da2deced
* https://scotch.io/courses/10-need-to-know-javascript-concepts/callbacks-promises-and-async

#### Event loop
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop
* https://medium.com/front-end-weekly/javascript-event-loop-explained-4cd26af121d4
* https://www.youtube.com/watch?v=8aGhZQkoFbQ


DOM - https://eloquentjavascript.net/14_dom.html

Event Handling - https://eloquentjavascript.net/15_event.html

How Browsers Work - https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/


### jQuery, AJAX
* Tutorial - http://jqfundamentals.com/chapter/jquery-basics
* Reference - https://learn.jquery.com


### NPM

* Will share resources...

### Drills

* https://learn.freecodecamp.org/
    1. Basic JavaScript
________


## Project:

See `js_chat_app`

